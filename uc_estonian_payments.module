<?php
/**
 * @file
 * Estonian Payments for Ubercart 7.
 */

/**
 * Implements hook_menu().
 *
 */
function uc_estonian_payments_menu() {
  $items['estonian_payments/return/%'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_estonian_payments_return',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['estonian_payments/cancel/%'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_estonian_payments_cancel',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_form_alter().
 */
function uc_estonian_payments_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id, $reset = FALSE);
    $banks = array('swedbank','seb','nordea','danskebank','lhv','krediidipank');
    if (in_array($order->payment_method, $banks)) {
      $id = $order->order_id;
      if (uc_estonian_payments_data_control($order->payment_method) != '') {
        drupal_set_message(uc_estonian_payments_data_control($order->payment_method), 'error');
        $form['actions']['submit'] = '';
      }
      else {
        $form = drupal_get_form('uc_estonian_payments_checkout_form', $id);
      }
    }
  }
}

/**
 * Set some values when sending data or getting from bank.
 */
function uc_estonian_payments_get_settings($key) {
  return array(
    'merchant_id' => variable_get("{$key}_merchant_id", ''),
    'banklink_url' => variable_get("{$key}_banklink_url", ''),
    'bank_public_key' => variable_get("{$key}_bank_public_key", ''),
    'merchant_private_key' => variable_get("{$key}_merchant_private_key", ''),
    'merchant_private_key_passphrase' => variable_get("{$key}_merchant_private_key_passphrase", ''),
  );
}

/**
 * Set cancel, reject and return urls.
 * get all the payment form values.
 * Create submit order and back buttons.
 * Put this all to the form.
 */
function uc_estonian_payments_checkout_form($form_state, $order_id) {
  $order = uc_order_load($order_id['build_info']['args'][0], $reset = FALSE);
  $bank = $order->payment_method;
  module_load_include('inc', 'uc_estonian_payments', 'inc/banklink');
  $return_url = "estonian_payments/return/{$bank}";
  $cancel_url = "estonian_payments/cancel/{$bank}";
  $settings = uc_estonian_payments_get_settings($bank);
  $payment = array(
    'order_id' => $order->order_id,
    'amount' => $order->order_total,
    'currency' => variable_get('uc_estonian_payments_currency_code', 'EUR'),
    'message' => t('Order nr. @num', array('@num' => $order->order_id)),
    'return' => url($return_url, array('absolute' => TRUE)),
    'cancel_return' => url($cancel_url, array('absolute' => TRUE)),
    // 'reject_return' => url($reject_url, array('absolute' => TRUE)),
  );
  $form = array();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Order'),
  );
  banklink_expand_payment_form($form, $settings + $payment, $payment, array('method_id' => $bank));
  return $form;
}

/**
 * Here we cant see payed sum inside query.
 * If we pay with Sampo SEB or Swedbank VK_STAMP contains Order id.
 */
function uc_estonian_payments_return($bank) {
  $key_test = '0';
  module_load_include('inc', 'uc_estonian_payments', 'inc/banklink');
  if (!empty($_REQUEST['VK_SERVICE'])) {
    // Set merchant public key for most settings variable.
    if ($_REQUEST['VK_SERVICE'] == 1111) {
      $bank_public_key = $bank . '_bank_public_key';
      $banklink_public_key = array('bank_public_key' => variable_get($bank_public_key, ''));
      $key_test = banklink_verify_signature($_REQUEST, $banklink_public_key);
    }
    else {
      return;
    }
  }
  // Test bank signature if results are not empty then create order else return.
  if ($key_test == 1) {
    $order = uc_order_load($_REQUEST['VK_STAMP']);
    $amount = $_REQUEST['VK_AMOUNT'];
    if ($order->order_status == 'in_checkout') {
      uc_estonian_payment_make_payment($order, $bank);
    }
    return t('Your payment is successfully completed. Thank You!');
  }
  else {
    return t('Something went wrong! Contact an administrator');
  }
}
/**
 * Estonian Payment make mayment.
 */
function uc_estonian_payment_make_payment($order, $bank) {
  if ($order->order_total > 0) {
    uc_payment_enter($order->order_id, $bank, $order->order_total, $order->uid, NULL, '');
    return uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  }
  else {
    drupal_set_message('Payment could not be verified.', 'error');
    watchdog('uc_estonian_payment', 'Payment could not be verified.', WATCHDOG_ERROR);
    drupal_goto();
  }
}

/**
 * If payment gets interrupted then just show some.
 */
function uc_estonian_payments_cancel($bank) {
  drupal_set_message(t('Payment has been cancelled.'), 'warning');
  drupal_goto();
}

/**
 * Additional helper functions.
 * Just broked this control into smaller pieces.
 */
function uc_estonian_payments_data_control($settings) {
  $out = '';
  if (variable_get("{$settings}_banklink_url", '') == '') {
    $out .= 'Missing bankink url <br />';
  }
  if (variable_get("{$settings}_merchant_id", '') == '') {
    $out .= 'Missing merchant id <br />';
  }
  if (variable_get("{$settings}_merchant_private_key", '') == '') {
    $out .= 'Missing merchant private key <br />';
  }
  if (variable_get("{$settings}_bank_public_key", '') == '') {
    $out .= 'Missing bank public key <br />';
  }
  return $out;
}

/**
 * Get icons.
 */
function estonian_payments_get_icon($bank_name) {
  $src = base_path() . drupal_get_path('module', 'uc_estonian_payments')
    . "/images/$bank_name.gif";
  $name = ucfirst($bank_name);
  return <<<html
<img style="vertical-align: middle" src="$src" alt="$name" />
html;
}
